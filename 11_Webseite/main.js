// ELEMENT ONCLICK
document.getElementById("bell").addEventListener("click", () => {
    notification()
})


// SEND THE NOTIFICATION
send_notif = (value) => {
    new Notification(value);
}


// CHECK FOR PERMISSION
notification = () => {
    if (Notification.permission === "granted") {
        send_notif("ORF News")
    }
    else if (Notification.permission !== 'denied' || Notification.permission === "default") {
        Notification.requestPermission(function (permission) {
            if (permission === "granted") {
                send_notif("ORF News")
            }
        });
    }
}